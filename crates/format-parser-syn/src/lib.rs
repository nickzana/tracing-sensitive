use parameters::Parameters;
use syn::{parse::Parse, parse::ParseStream};
use template::Template;

// TODO: Improve on error messages; ensure that span is persisited when parsed
// TODO: Is it possible to provide custom hints/recommended changes? Does rust_analyzer handle that
// or clippy? Is it parsed from the compiler error?

pub mod template {

    //! # Templates
    //! Template strings, or format strings, are the first argument to `std::fmt::format`. They are
    //! always static string literals, as enforced by the compiler, in order to perform validity
    //! checking.
    //!
    //! The `Template` struct holds a `stream` of `Expr`s representing the inner contents of the
    //! format string.

    use syn::{parse::Parse, LitStr, __private::ToTokens};

    /// A data type to hold a format template.
    pub struct Template {
        pub stream: Vec<Expr>,
    }

    impl IntoIterator for Template {
        type Item = Expr;
        type IntoIter = std::vec::IntoIter<Self::Item>;

        fn into_iter(self) -> Self::IntoIter {
            self.stream.into_iter()
        }
    }

    impl TryFrom<LitStr> for Template {
        // TODO: Is it worth creating a wrapper error for custom errors?
        type Error = syn::Error;

        fn try_from(value: LitStr) -> Result<Self, Self::Error> {
            // Split on { and }, ignoring {{ and }}
            // Parse elements into Expr
            todo!()
        }
    }

    impl Parse for Template {
        fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
            input.parse::<LitStr>()?.try_into()
        }
    }

    /// A data type to parse expressions within template strings.
    pub enum Expr {
        /// Represents any non-parameter tokens within the template string
        // TODO: Example; what kind of Expr will it be?
        Literal(syn::Expr),

        /// A template parameter within the string literal.
        ///
        /// # Examples
        /// ```
        /// let parameter = Expr::Parameter(Parameter::Positional(0));
        ///
        /// assert_eq!(parameter, syn::parse_str::<Expr>("{0}"));
        /// ```
        Parameter(Parameter),
    }

    impl Parse for Expr {
        fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
            todo!()
        }
    }

    pub enum Parameter {
        Named { name: LitStr },
        Positional(i32),
    }

    mod format {}

    pub enum DebugHexFormat {}

    pub enum Format {
        Display,
        Debug { hex_format: DebugHexFormat },
        Octal,
        LowerHex,
        UpperHex,
        Pointer,
        Binary,
        LowerExp,
        UpperExp,
    }
}

pub mod argument {
    use syn::{parse::Parse, Expr, Ident};

    pub enum Argument {
        Named { name: Ident, value: Expr },
        Positional(Expr),
    }

    impl Parse for Argument {
        fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
            todo!()
        }
    }
}

pub mod parameters {
    use crate::argument::Argument;
    use std::collections::HashMap;
    use syn::{parse::Parse, Ident};

    /// A collection of `argument::Argument`s parsed as both `positional` and `named` parameters.
    pub struct Parameters {
        pub positional: Vec<Argument>,
        pub named: HashMap<Ident, Argument>,
    }

    impl Parse for Parameters {
        fn parse(input: syn::parse::ParseStream) -> syn::Result<Self> {
            todo!()
        }
    }
}

/// A sequence of macro arguments conforming `std::fmt::format` syntax extension.
///
/// Every `FormatArgs` must have a `template`, which is a string literal and the first argument to
/// the `format!` macro. The following arguments are then parsed as `Parameters` that are formatted
/// as per the `template`.
///
/// By using the default `syn::parse::Parse` implementation, the arguments are validated to be
/// correct syntax for `std::fmt::format`, as per the grammar laid out in the `std::fmt`
/// documentation. More specifically, it enforces that the template string's formatting parameters
/// are valid and match the number and form of remaining arguments, in addition to other rules of
/// the syntax extension. For more details about validation, see the documentation for
/// `FormatArgs::is_valid`. An unvalidated implementation is also provided by
/// `FormatArgs::parse_unvalidated`.
///
/// For more details about the syntax extension, including a formal grammar definition, see the
/// documentation for `std::fmt`.
pub struct FormatArgs {
    pub template: Template,
    pub parameters: Parameters,
}

impl FormatArgs {
    // NOTE: "A format string is required to use all of its arguments, otherwise it is a
    // compile-time error. You may refer to the same argument more than once in the format string."
    // - https://doc.rust-lang.org/std/fmt/index.html#positional-parameters
    //
    // NOTE: "It is not valid to put positional parameters (those without names) after arguments
    // that have names. Like with positional arguments, it is not valid to provide named parameters
    // that are unused by the format string." -
    // https://doc.rust-lang.org/std/fmt/index.html#named-parameters
    pub fn is_valid() -> bool {
        todo!()
    }

    pub fn parse_unvalidated(input: ParseStream<'_>) -> syn::Result<Self> {
        todo!()
    }
}

impl Parse for FormatArgs {
    fn parse(input: ParseStream) -> syn::Result<Self> {
        todo!()
    }
}
