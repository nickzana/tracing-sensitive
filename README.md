# tracing-sensitive

A small library that forces a user of the tracing library to declare formatted
string parameters as sensitive or insensitive in order to allow for replacement
of sensitive strings with `[redacted]`.

`tracing-sensitive` works by using a `tracing_sensitive::Subscriber` wrapper
type around a `tracing::Subscriber`. `tracing_sensitive::Subscriber` is generic
over a type `Sensitivity`, which can either be `tracing_sensitive::Redacted` or
`tracing_sensitive::Allowed`. This determines whether the logs will be
`Redacted` or `Allowed` to have sensitive information in them.

When a logging macro from `tracing_sensitive` is invoked, the sensitive
arguments are replaced with `[redacted]` if the global `Subscriber` is a
`Subscriber<Redacted>`, then passed to the corresponding `tracing` log macros.

# Usage

See `examples/basic.rs` for a full working example.

## Cargo.toml
```rust
[dependencies]
tracing-sensitive = "0.1"
```

## Setup Subscriber

Set up a global subscriber before any log functions are called.

```rust
use tracing_subscriber::{fmt, prelude::*, registry};
use tracing_sensitive::subscriber::{Redacted, Subscriber};

let registry = registry().with(fmt::Layer::default());
let subscriber = Subscriber::<Redacted>::new(registry);
subscriber.init();
```

## Log

Import the logging macro from `tracing_sensitive` and call it using normal
argument formatting, wrapping every argument in an explicity `Sensitive` or
`Insensitive` sensitivity.

```rust
use tracing_sensitive::{info, Insensitive, Sensitive};

// Logs "Hello, Alice!" if global Subscriber is Subscriber<Allowed> or
// "Hello, [redacted]!" if global Subscriber is Subscriber<Redacted>
info!("Hello, {}!", Sensitive(&"Alice"));

// Always logs "Hello, World!"
info!("Hello, {}!", Insensitive(&"World"));
```
