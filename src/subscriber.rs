use crate::{sensitivity::Sensitivity, Allow, Redact};
use tracing::{dispatcher::get_default, span};

pub struct Redacted;
pub struct Allowed;

pub trait Marker {}
impl Marker for Redacted {}
impl Marker for Allowed {}

pub trait AnySubscriber: tracing::Subscriber + Send + Sync + 'static {}
impl<T> AnySubscriber for T where T: tracing::Subscriber + Send + Sync + 'static + Sized {}

pub struct Subscriber<Sensitivity = Redacted>
where
    Sensitivity: Marker + 'static,
{
    sensitivity: std::marker::PhantomData<Sensitivity>,
    inner: Box<dyn AnySubscriber>,
}

impl<T: Marker> Subscriber<T> {
    pub fn new<K: AnySubscriber>(inner: K) -> Self {
        Self {
            inner: Box::new(inner),
            sensitivity: std::marker::PhantomData,
        }
    }
}

impl<T: Marker> tracing::Subscriber for Subscriber<T> {
    fn enabled(&self, metadata: &tracing::Metadata<'_>) -> bool {
        self.inner.enabled(metadata)
    }

    fn new_span(&self, span: &span::Attributes<'_>) -> span::Id {
        self.inner.new_span(span)
    }

    fn record(&self, span: &span::Id, values: &span::Record<'_>) {
        self.inner.record(span, values)
    }

    fn record_follows_from(&self, span: &span::Id, follows: &span::Id) {
        self.inner.record_follows_from(span, follows)
    }

    fn event(&self, event: &tracing::Event<'_>) {
        self.inner.event(event)
    }

    fn enter(&self, span: &span::Id) {
        self.inner.enter(span)
    }

    fn exit(&self, span: &span::Id) {
        self.inner.exit(span)
    }
}

pub fn get_sensitivity() -> Sensitivity {
    get_default(|dispatch| -> Sensitivity {
        if dispatch.is::<Subscriber<Allowed>>() {
            Allow
        } else if dispatch.is::<Subscriber<Redacted>>() {
            Redact
        } else {
            Sensitivity::default()
        }
    })
}
