pub use info::Info::*;
pub use sensitivity::Sensitivity::*;
pub use subscriber::{Allowed, Redacted, Subscriber};
pub use tracing::{event, Level};

pub mod info;
pub mod subscriber;

pub mod sensitivity {

    use serde::{Deserialize, Serialize};

    #[derive(Clone, Copy, Debug, Eq, PartialEq, Serialize, Deserialize)]
    pub enum Sensitivity {
        Allow,
        Redact,
    }

    impl Default for Sensitivity {
        fn default() -> Self {
            Self::Redact
        }
    }
}

#[macro_export]
macro_rules! sensitivity {
    ($s:expr, $v:expr) => {
        $v.to_log_string($s)
    };
    ($s:expr, $v:expr, $($rest:expr),*) => {
        $v.to_log_string($s), sensitivity!($s, $($rest),*)
    };
}

#[macro_export]
macro_rules! log {
    ($lvl:expr, $fmt:expr) => {
        $crate::event!($lvl, $fmt)
    };
    ($lvl:expr, $s:expr, $fmt:expr) => {
        $crate::event!($lvl, $fmt)
    };
    ($lvl:expr, $s:expr, $fmt:expr, $($args:expr),*) => {
        $crate::event!($lvl, $fmt, $crate::sensitivity!($s, $($args),*))
    };
}

#[macro_export]
macro_rules! info {
        ($fmt:expr) => {
            $crate::log!($crate::Level::INFO, $fmt)
        };
        ($fmt:expr, $($args:expr),*) => {
            $crate::log!($crate::Level::INFO, $crate::subscriber::get_sensitivity(), $fmt, $($args),*)
        };
}

#[macro_export]
macro_rules! debug {
        ($fmt:expr) => {
            $crate::log!($crate::Level::DEBUG, $fmt)
        };
        ($fmt:expr, $($args:expr),*) => {
            $crate::log!($crate::Level::DEBUG, $crate::subscriber::get_sensitivity(), $fmt, $($args),*)
        };
}

#[macro_export]
macro_rules! warn {
        ($fmt:expr) => {
            $crate::log!($crate::Level::WARN, $fmt)
        };
        ($fmt:expr, $($args:expr),*) => {
            $crate::log!($crate::Level::WARN, $crate::subscriber::get_sensitivity(), $fmt, $($args),*)
        };
}

#[macro_export]
macro_rules! trace {
        ($fmt:expr) => {
            $crate::log!($crate::Level::TRACE, $fmt)
        };
        ($fmt:expr, $($args:expr),*) => {
            $crate::log!($crate::Level::TRACE, $crate::subscriber::get_sensitivity(), $fmt, $($args),*)
        };
}

#[macro_export]
macro_rules! error {
        ($fmt:expr) => {
            $crate::log!($crate::Level::ERROR, $fmt)
        };
        ($fmt:expr, $($args:expr),*) => {
            $crate::log!($crate::Level::ERROR, $crate::subscriber::get_sensitivity(), $fmt, $($args),*)
        };
}

#[cfg(test)]
mod tests {
    #[allow(unused_imports)]
    use crate::{log, Allow, Insensitive, Redact, Sensitive};
    use tracing::Level;
    use tracing_test::traced_test;

    #[traced_test]
    #[tokio::test]
    async fn test_log_static() {
        log!(
            Level::INFO,
            "This is an insensitive log without sensitivity parameter."
        );
        assert!(logs_contain(
            "This is an insensitive log without sensitivity parameter."
        ));
    }

    #[traced_test]
    #[tokio::test]
    async fn test_log_static_allow() {
        log!(
            Level::INFO,
            Allow,
            "This is an insensitive log with Allow parameter."
        );
        assert!(logs_contain(
            "This is an insensitive log with Allow parameter."
        ));
    }

    #[traced_test]
    #[tokio::test]
    async fn test_log_static_redact() {
        log!(
            Level::INFO,
            "This is an insensitive log with Redact parameter."
        );
        assert!(logs_contain(
            "This is an insensitive log with Redact parameter."
        ));
    }

    #[traced_test]
    #[tokio::test]
    async fn test_log_allowed_sensitive() {
        log!(Level::INFO, Allow, "Hello, {}!", Sensitive(&"Alice"));
        assert!(logs_contain("Hello, Alice!"));
    }

    #[traced_test]
    #[tokio::test]
    async fn test_log_redacted_sensitive() {
        log!(Level::INFO, Redact, "Hello, {}!", Sensitive(&"Alice"));
        assert!(logs_contain("Hello, [redacted]!"));
        assert!(!logs_contain("Alice"));
    }

    #[traced_test]
    #[tokio::test]
    async fn test_log_allowed_insensitive() {
        log!(
            Level::INFO,
            Allow,
            "Hello, allowed {}!",
            Insensitive(&"Arti")
        );
        assert!(logs_contain("Hello, allowed Arti!"));
    }

    #[traced_test]
    #[tokio::test]
    async fn test_log_redacted_insensitive() {
        log!(
            Level::INFO,
            Redact,
            "Hello, redacted {}!",
            Insensitive(&"Arti")
        );
        assert!(logs_contain("Hello, redacted Arti!"));
    }
}
