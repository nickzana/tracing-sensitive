use crate::{sensitivity::Sensitivity, Allow, Redact};
use std::fmt::Display;
use Info::*;

/// A structure to hold information and force it to be explicitly marked
/// as sensitive or insensitive.
pub enum Info<'a, T: Display> {
    Sensitive(&'a T),
    Insensitive(&'a T),
}

impl<'a, T: Display> Info<'a, T> {
    pub fn to_log_string(self, s: Sensitivity) -> String {
        match self {
            Insensitive(i) => i.to_string(),
            Sensitive(i) => match s {
                Allow => i.to_string(),
                Redact => "[redacted]".to_string(),
            },
        }
    }
}
