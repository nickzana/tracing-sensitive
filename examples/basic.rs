extern crate tracing_sensitive;

use tracing_sensitive::{
    info,
    subscriber::{Redacted, Subscriber},
    Insensitive, Sensitive,
};
use tracing_subscriber::{fmt, prelude::*, registry};

fn main() {
    let registry = registry().with(fmt::Layer::default());
    let subscriber = Subscriber::<Redacted>::new(registry);
    subscriber.init();

    info!("Hello, {}!", Sensitive(&"Alice"));
    info!("Hello, {}!", Insensitive(&"Arti"));
}
